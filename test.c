/*
 * test.c
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Hardware Resource Detection single header library test app
 *
 * Compilation: gcc test.c -o test
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* the two hooks you provide */
void mem_exclude(uint64_t base, uint64_t size) {
    printf("unusable memory %lx %ld\n", base, size);
}
void drv_resource(char *name, char *driver, char *altdriver, int type, uint64_t arg1, uint64_t arg2)
{
    char *types[] = { "none  ", "CPU   ", "IOport", "IRQ   ", "DMA   ", "MMIO  ", "PCI   ", "EC    ", "SMB   ", "CMOS  " };
    printf("device '%s' driver '%s' (alt '%s') type %d %s arguments %lx %ld\n", name, driver, altdriver,
        type, type < (int)(sizeof(types)/sizeof(types[0])) ? types[type] : "???", arg1, arg2);
}

/* include the library */
#define HWDET_RESVMEM  mem_exclude
#define HWDET_RESOURCE drv_resource
#include "hwdet.h"

/* a minimal example that reads tables from files (you probably want to locate ACPI tables in memory and pass addresses of those) */
int main(int argc, char *argv[])
{
    FILE *f;
    uint8_t *ds[16] = { 0 };
    int size, i, num = 0;

    /* parse arguments */
    if(argc < 2) {
        printf("HwDet, Copyright (c) 2023 bzt, GPLv3+\nhttps://gitlab.com/bztsrc/hwdet\n\n");
        printf("%s file [file [file [...]]]\n\n", argv[0]);
        printf("Files can be ACPI DSDT/SSDT tables, FDT (dtb) or GUDT blobs.\n");
        return 1;
    }
    for(i = 1; i < argc && i < 16; i++) {
        f = fopen(argv[i], "rb");
        if(f) {
            fseek(f, 0, SEEK_END);
            size = (int)ftell(f);
            fseek(f, 0, SEEK_SET);
            ds[num] = malloc(size);
            if(ds[num]) size = fread(ds[num++], 1, size, f);
            fclose(f);
        }
    }

    /* call the library */
    hwdet(num, ds);

    /* free resources */
    for(i = 0; i < num; i++) if(ds[i]) free(ds[i]);
    printf("Done.\n");
    return 0;
}
