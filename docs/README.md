Example outputs
===============

[[_TOC_]]

Bochs
-----

```
$ ./test bochs.DSDT bochs.SSDT
device 'PCI0' driver 'PNP0A03' (alt '') type 2 IOport arguments cf8 1
device 'PCI0' driver 'PNP0A03' (alt '') type 2 IOport arguments d00 62208
unusable memory a0000 131072
device 'PCI0' driver 'PNP0A03' (alt '') type 5 MMIO   arguments a0000 131072
unusable memory c0000000 1052770304
device 'PCI0' driver 'PNP0A03' (alt '') type 5 MMIO   arguments c0000000 1052770304
unusable memory fed00000 1024
device 'HPET' driver 'PNP0103' (alt '') type 5 MMIO   arguments fed00000 1024
device 'RTC_' driver 'PNP0B00' (alt '') type 2 IOport arguments 70 16
device 'RTC_' driver 'PNP0B00' (alt '') type 3 IRQ    arguments 8 0
device 'RTC_' driver 'PNP0B00' (alt '') type 2 IOport arguments 72 2
device 'KBD_' driver 'PNP0303' (alt '') type 2 IOport arguments 60 1
device 'KBD_' driver 'PNP0303' (alt '') type 2 IOport arguments 64 1
device 'KBD_' driver 'PNP0303' (alt '') type 3 IRQ    arguments 1 0
device 'MOU_' driver 'PNP0F13' (alt '') type 3 IRQ    arguments c 0
device 'FDC0' driver 'PNP0700' (alt '') type 2 IOport arguments 3f2 1
device 'FDC0' driver 'PNP0700' (alt '') type 2 IOport arguments 3f7 1
device 'FDC0' driver 'PNP0700' (alt '') type 3 IRQ    arguments 6 0
device 'FDC0' driver 'PNP0700' (alt '') type 4 DMA    arguments 2 1
device 'LPT_' driver 'PNP0400' (alt '') type 2 IOport arguments 378 8
device 'LPT_' driver 'PNP0400' (alt '') type 3 IRQ    arguments 7 0
device 'COM1' driver 'PNP0501' (alt '') type 2 IOport arguments 3f8 1
device 'COM1' driver 'PNP0501' (alt '') type 3 IRQ    arguments 4 0
device 'COM2' driver 'PNP0501' (alt '') type 2 IOport arguments 2f8 1
device 'COM2' driver 'PNP0501' (alt '') type 3 IRQ    arguments 3 0
device 'LNKA' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKB' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKC' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKD' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'CPU0' driver 'CORE' (alt '') type 1 CPU    arguments 0 0
device 'CPU1' driver 'CORE' (alt '') type 1 CPU    arguments 0 1
device 'CPU2' driver 'CORE' (alt '') type 1 CPU    arguments 0 2
device 'CPU3' driver 'CORE' (alt '') type 1 CPU    arguments 0 3
Done.
```

Qemu (BIOS)
-----------

```
$ ./test qemu.DSDT
device 'LNKA' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKB' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKC' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKD' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKS' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 9 0
unusable memory fed00000 1024
unusable memory fed00000 1024
device 'HPET' driver 'PNP0103' (alt '') type 5 MMIO   arguments fed00000 1024
device 'PRES' driver 'PNP0A06' (alt '') type 2 IOport arguments af00 12
device 'C000' driver 'CORE' (alt '') type 1 CPU    arguments 0 0
device 'GPE0' driver 'PNP0A06' (alt '') type 2 IOport arguments afe0 4
device 'PHPR' driver 'PNP0A06' (alt '') type 2 IOport arguments ae00 24
device 'FWCF' driver 'QEMU0002' (alt '') type 2 IOport arguments 510 12
device 'KBD_' driver 'PNP0303' (alt '') type 2 IOport arguments 60 1
device 'KBD_' driver 'PNP0303' (alt '') type 2 IOport arguments 64 1
device 'KBD_' driver 'PNP0303' (alt '') type 3 IRQ    arguments 1 0
device 'MOU_' driver 'PNP0F13' (alt '') type 3 IRQ    arguments c 0
device 'FLPA' driver 'PNP0700' (alt '') type 2 IOport arguments 3f2 4
device 'FLPA' driver 'PNP0700' (alt '') type 2 IOport arguments 3f7 1
device 'FLPA' driver 'PNP0700' (alt '') type 3 IRQ    arguments 6 0
device 'FLPA' driver 'PNP0700' (alt '') type 4 DMA    arguments 2 1
device 'LPT1' driver 'PNP0400' (alt '') type 2 IOport arguments 378 8
device 'LPT1' driver 'PNP0400' (alt '') type 3 IRQ    arguments 7 0
device 'COM1' driver 'PNP0501' (alt '') type 2 IOport arguments 3f8 8
device 'COM1' driver 'PNP0501' (alt '') type 3 IRQ    arguments 4 0
device 'RTC_' driver 'PNP0B00' (alt '') type 2 IOport arguments 70 8
device 'RTC_' driver 'PNP0B00' (alt '') type 3 IRQ    arguments 8 0
Done.
```

Qemu (UEFI)
-----------

```
$ ./test qemu_efi.DSDT qemu_efi.SSDT
device 'LNKS' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 9 0
device 'DMAC' driver 'PNP0200' (alt '') type 2 IOport arguments 81 1
device 'DMAC' driver 'PNP0200' (alt '') type 2 IOport arguments 87 1
device 'DMAC' driver 'PNP0200' (alt '') type 2 IOport arguments 89 1
device 'DMAC' driver 'PNP0200' (alt '') type 2 IOport arguments 8f 1
device 'DMAC' driver 'PNP0200' (alt '') type 2 IOport arguments c0 1
device 'DMAC' driver 'PNP0200' (alt '') type 4 DMA    arguments 4 1
device 'TMR_' driver 'PNP0100' (alt '') type 2 IOport arguments 40 1
device 'TMR_' driver 'PNP0100' (alt '') type 3 IRQ    arguments 0 0
device 'RTC_' driver 'PNP0B00' (alt '') type 2 IOport arguments 70 1
device 'RTC_' driver 'PNP0B00' (alt '') type 3 IRQ    arguments 8 0
device 'SPKR' driver 'PNP0800' (alt '') type 2 IOport arguments 61 1
device 'FPU_' driver 'PNP0C04' (alt '') type 2 IOport arguments f0 1
device 'FPU_' driver 'PNP0C04' (alt '') type 3 IRQ    arguments d 0
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 10 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 22 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 44 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 62 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 65 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 72 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 80 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 84 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 88 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 8c 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 90 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments a2 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments e0 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 1e0 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 160 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 278 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 370 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 378 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 402 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 440 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 678 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments 778 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments afe0 1
device 'XTRA' driver 'PNP0C02' (alt '') type 2 IOport arguments b000 1
unusable memory fec00000 4096
device 'XTRA' driver 'PNP0C02' (alt '') type 5 MMIO   arguments fec00000 4096
unusable memory fee00000 1048576
device 'XTRA' driver 'PNP0C02' (alt '') type 5 MMIO   arguments fee00000 1048576
device 'PS2K' driver 'PNP0303' (alt 'PNP030B') type 2 IOport arguments 60 1
device 'PS2K' driver 'PNP0303' (alt 'PNP030B') type 2 IOport arguments 64 1
device 'PS2K' driver 'PNP0303' (alt 'PNP030B') type 3 IRQ    arguments 1 0
device 'PS2M' driver 'PNP0F03' (alt 'PNP0F13') type 3 IRQ    arguments c 0
device 'UAR1' driver 'PNP0501' (alt '') type 2 IOport arguments 3f8 1
device 'UAR1' driver 'PNP0501' (alt '') type 3 IRQ    arguments 4 0
device 'UAR2' driver 'PNP0501' (alt '') type 2 IOport arguments 2f8 1
device 'UAR2' driver 'PNP0501' (alt '') type 3 IRQ    arguments 3 0
device 'FDC_' driver 'PNP0700' (alt '') type 2 IOport arguments 3f0 1
device 'FDC_' driver 'PNP0700' (alt '') type 2 IOport arguments 3f7 1
device 'FDC_' driver 'PNP0700' (alt '') type 3 IRQ    arguments 6 0
device 'FDC_' driver 'PNP0700' (alt '') type 4 DMA    arguments 2 1
device 'PAR1' driver 'PNP0400' (alt '') type 2 IOport arguments 378 1
device 'PAR1' driver 'PNP0400' (alt '') type 3 IRQ    arguments 7 0
unusable memory 3f25f98 48
Done.
```

Real life PC DSDT
-----------------

```
$ ./test /sys/firmware/acpi/tables/DSDT
unusable memory fed81500 1023
unusable memory fed80000 4096
unusable memory cc7fa018 9
unusable memory f0000000 134217728
device 'AMDN' driver 'PNP0C01' (alt 'PNP0A03') type 5 MMIO   arguments f0000000 134217728
device 'COM2' driver 'PNP0501' (alt '') type 2 IOport arguments 2f8 8
device 'COM2' driver 'PNP0501' (alt '') type 3 IRQ    arguments 3 0
unusable memory feb80000 524288
device 'IOMA' driver 'PNP0C02' (alt '') type 5 MMIO   arguments feb80000 524288
unusable memory fd000000 16777216
device 'APSP' driver 'PNP0C02' (alt '') type 5 MMIO   arguments fd000000 16777216
device 'DMAD' driver 'PNP0200' (alt '') type 4 DMA    arguments 4 1
device 'DMAD' driver 'PNP0200' (alt '') type 2 IOport arguments 81 3
device 'DMAD' driver 'PNP0200' (alt '') type 2 IOport arguments 87 1
device 'DMAD' driver 'PNP0200' (alt '') type 2 IOport arguments 89 3
device 'DMAD' driver 'PNP0200' (alt '') type 2 IOport arguments 8f 1
device 'DMAD' driver 'PNP0200' (alt '') type 2 IOport arguments c0 32
device 'TMR_' driver 'PNP0100' (alt '') type 2 IOport arguments 40 4
device 'TMR_' driver 'PNP0100' (alt '') type 3 IRQ    arguments 0 0
device 'RTC0' driver 'PNP0B00' (alt '') type 2 IOport arguments 70 2
device 'RTC0' driver 'PNP0B00' (alt '') type 3 IRQ    arguments 8 0
device 'SPKR' driver 'PNP0800' (alt '') type 2 IOport arguments 61 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 10 16
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 22 30
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 63 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 65 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 67 9
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 72 14
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 80 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 84 3
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 88 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 8c 3
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 90 16
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments a2 30
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments b1 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments e0 16
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 4d0 2
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 40b 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 4d6 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments c00 2
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments c14 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments c50 2
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments c52 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments c6c 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments c6f 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments cd0 2
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments cd2 2
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments cd4 2
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments cd6 2
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments cd8 8
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 800 160
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments b00 16
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments b20 32
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 900 16
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 910 16
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 60 1
device 'S900' driver 'PNP0C02' (alt '') type 2 IOport arguments 64 1
unusable memory fec00000 4096
device 'S900' driver 'PNP0C02' (alt '') type 5 MMIO   arguments fec00000 4096
unusable memory fec01000 4096
device 'S900' driver 'PNP0C02' (alt '') type 5 MMIO   arguments fec01000 4096
unusable memory fedc0000 4096
device 'S900' driver 'PNP0C02' (alt '') type 5 MMIO   arguments fedc0000 4096
unusable memory fee00000 4096
device 'S900' driver 'PNP0C02' (alt '') type 5 MMIO   arguments fee00000 4096
unusable memory fed80000 65536
device 'S900' driver 'PNP0C02' (alt '') type 5 MMIO   arguments fed80000 65536
unusable memory fec10000 4096
device 'S900' driver 'PNP0C02' (alt '') type 5 MMIO   arguments fec10000 4096
unusable memory ff000000 16777216
device 'S900' driver 'PNP0C02' (alt '') type 5 MMIO   arguments ff000000 16777216
device 'EC0_' driver 'PNP0C09' (alt '') type 2 IOport arguments 62 1
device 'EC0_' driver 'PNP0C09' (alt '') type 2 IOport arguments 66 1
device 'CIR0' driver 'ITE8708' (alt '') type 2 IOport arguments 280 16
device 'CIR0' driver 'ITE8708' (alt '') type 3 IRQ    arguments a 0
device 'LNKA' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKB' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKC' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKD' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKE' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKF' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKG' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
device 'LNKH' driver 'PNP0C0F' (alt '') type 3 IRQ    arguments 0 0
unusable memory fed80000 8388608
unusable memory 1000000b7 256
unusable memory fed80d00 256
unusable memory fed81e00 256
unusable memory fedd5800 304
unusable memory fedd5800 304
unusable memory fedc0020 4
unusable memory fec11000 256
device 'HFP1' driver 'AMDI0060' (alt '') type 5 MMIO   arguments fec11000 256
device 'GPIO' driver 'AMDI0030' (alt '') type 3 IRQ    arguments 7 0
unusable memory fed81500 1024
device 'GPIO' driver 'AMDI0030' (alt '') type 5 MMIO   arguments fed81500 1024
unusable memory fed81200 256
device 'GPIO' driver 'AMDI0030' (alt '') type 5 MMIO   arguments fed81200 256
device 'FUR0' driver 'AMDI0020' (alt '') type 3 IRQ    arguments 3 0
unusable memory fedc9000 4096
device 'FUR0' driver 'AMDI0020' (alt '') type 5 MMIO   arguments fedc9000 4096
unusable memory fedc7000 4096
device 'FUR0' driver 'AMDI0020' (alt '') type 5 MMIO   arguments fedc7000 4096
device 'FUR1' driver 'AMDI0020' (alt '') type 3 IRQ    arguments 4 0
unusable memory fedca000 4096
device 'FUR1' driver 'AMDI0020' (alt '') type 5 MMIO   arguments fedca000 4096
unusable memory fedc8000 4096
device 'FUR1' driver 'AMDI0020' (alt '') type 5 MMIO   arguments fedc8000 4096
device 'FUR2' driver 'AMDI0020' (alt '') type 3 IRQ    arguments 3 0
unusable memory fedce000 4096
device 'FUR2' driver 'AMDI0020' (alt '') type 5 MMIO   arguments fedce000 4096
unusable memory fedcc000 4096
device 'FUR2' driver 'AMDI0020' (alt '') type 5 MMIO   arguments fedcc000 4096
device 'FUR3' driver 'AMDI0020' (alt '') type 3 IRQ    arguments 4 0
unusable memory fedcf000 4096
device 'FUR3' driver 'AMDI0020' (alt '') type 5 MMIO   arguments fedcf000 4096
unusable memory fedcd000 4096
device 'FUR3' driver 'AMDI0020' (alt '') type 5 MMIO   arguments fedcd000 4096
device 'I2CA' driver 'AMDI0010' (alt '') type 3 IRQ    arguments a 0
unusable memory fedc2000 4096
device 'I2CA' driver 'AMDI0010' (alt '') type 5 MMIO   arguments fedc2000 4096
device 'I2CB' driver 'AMDI0010' (alt '') type 3 IRQ    arguments b 0
unusable memory fedc3000 4096
device 'I2CB' driver 'AMDI0010' (alt '') type 5 MMIO   arguments fedc3000 4096
device 'I2CC' driver 'AMDI0010' (alt '') type 3 IRQ    arguments 4 0
unusable memory fedc4000 4096
device 'I2CC' driver 'AMDI0010' (alt '') type 5 MMIO   arguments fedc4000 4096
device 'I2CD' driver 'AMDI0010' (alt '') type 3 IRQ    arguments 6 0
unusable memory fedc5000 4096
device 'I2CD' driver 'AMDI0010' (alt '') type 5 MMIO   arguments fedc5000 4096
device 'I2CE' driver 'AMDI0010' (alt '') type 3 IRQ    arguments e 0
unusable memory fedc6000 4096
device 'I2CE' driver 'AMDI0010' (alt '') type 5 MMIO   arguments fedc6000 4096
device 'I2CF' driver 'AMDI0010' (alt '') type 3 IRQ    arguments f 0
unusable memory fedcb000 4096
device 'I2CF' driver 'AMDI0010' (alt '') type 5 MMIO   arguments fedcb000 4096
device 'EMM0' driver 'AMDI0040' (alt '') type 3 IRQ    arguments 5 0
unusable memory fedd5000 4096
device 'EMM0' driver 'AMDI0040' (alt '') type 5 MMIO   arguments fedd5000 4096
device 'UAR1' driver 'PNP0500' (alt '') type 2 IOport arguments 2e8 8
device 'UAR1' driver 'PNP0500' (alt '') type 3 IRQ    arguments 3 0
device 'UAR2' driver 'PNP0500' (alt '') type 2 IOport arguments 2f8 8
device 'UAR2' driver 'PNP0500' (alt '') type 3 IRQ    arguments 4 0
device 'UAR3' driver 'PNP0500' (alt '') type 2 IOport arguments 3e8 8
device 'UAR3' driver 'PNP0500' (alt '') type 3 IRQ    arguments 3 0
device 'UAR4' driver 'PNP0500' (alt '') type 2 IOport arguments 3f8 8
device 'UAR4' driver 'PNP0500' (alt '') type 3 IRQ    arguments 4 0
device 'HPET' driver 'PNP0103' (alt '') type 3 IRQ    arguments 0 0
device 'HPET' driver 'PNP0103' (alt '') type 3 IRQ    arguments 8 0
unusable memory fed00000 1024
device 'HPET' driver 'PNP0103' (alt '') type 5 MMIO   arguments fed00000 1024
unusable memory fed40000 20480
unusable memory fed40000 72
unusable memory fed40000 20480
device '_TPM' driver 'PNP0C31' (alt '') type 5 MMIO   arguments fed40000 20480
unusable memory ffff0000 240
unusable memory cc6c7018 65536
unusable memory cc646018 256
unusable memory cc6b6018 65536
Done.
```

Raspberry Pi 3
--------------

```
$ ./test bcm2710-rpi-3-b-plus.dtb
unusable memory 0 4096
unusable memory 7e101000 8192
device 'cprman@7e101000' driver 'brcm,bcm2835-cprman' (alt '') type 5 MMIO   arguments 7e101000 8192
unusable memory 7e00b880 64
device 'mailbox@7e00b880' driver 'brcm,bcm2835-mbox' (alt '') type 5 MMIO   arguments 7e00b880 64
unusable memory 7e200000 180
device 'gpio@7e200000' driver 'brcm,bcm2835-gpio' (alt '') type 5 MMIO   arguments 7e200000 180
unusable memory 7e201000 512
device 'serial@7e201000' driver 'arm,pl011' (alt 'arm,primecell') type 5 MMIO   arguments 7e201000 512
unusable memory 7e202000 256
device 'mmc@7e202000' driver 'brcm,bcm2835-sdhost' (alt '') type 5 MMIO   arguments 7e202000 256
device 'spidev@0' driver 'spidev' (alt '') type 5 MMIO   arguments 0 0
device 'spidev@1' driver 'spidev' (alt '') type 5 MMIO   arguments 1 0
unusable memory 7e215000 8
device 'aux@7e215000' driver 'brcm,bcm2835-aux' (alt '') type 5 MMIO   arguments 7e215000 8
unusable memory 7e215040 64
device 'serial@7e215040' driver 'brcm,bcm2835-aux-uart' (alt '') type 5 MMIO   arguments 7e215040 64
unusable memory 7e980000 65536
device 'usb@7e980000' driver 'brcm,bcm2708-usb' (alt '') type 5 MMIO   arguments 7e980000 65536
unusable memory 7e006000 4096
device 'usb@7e980000' driver 'brcm,bcm2708-usb' (alt '') type 5 MMIO   arguments 7e006000 4096
device 'usb-port@1' driver 'usb424,2514' (alt '') type 5 MMIO   arguments 1 0
device 'usb-port@1' driver 'usb424,2514' (alt '') type 5 MMIO   arguments 1 0
device 'ethernet@1' driver 'usb424,7800' (alt '') type 5 MMIO   arguments 1 0
unusable memory 7e007000 3840
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 5 MMIO   arguments 7e007000 3840
unusable memory 7e00b200 512
device 'interrupt-controller@7e00b200' driver 'brcm,bcm2836-armctrl-ic' (alt '') type 5 MMIO   arguments 7e00b200 512
unusable memory 7e100000 276
device 'watchdog@7e100000' driver 'brcm,bcm2835-pm' (alt 'brcm,bcm2835-pm-wdt') type 5 MMIO   arguments 7e100000 276
unusable memory 7e00a000 36
device 'watchdog@7e100000' driver 'brcm,bcm2835-pm' (alt 'brcm,bcm2835-pm-wdt') type 5 MMIO   arguments 7e00a000 36
unusable memory 7e104000 16
device 'rng@7e104000' driver 'brcm,bcm2835-rng' (alt '') type 5 MMIO   arguments 7e104000 16
unusable memory 7e212000 8
device 'thermal@7e212000' driver 'brcm,bcm2837-thermal' (alt '') type 5 MMIO   arguments 7e212000 8
unusable memory 40000000 256
device 'local_intc@40000000' driver 'brcm,bcm2836-l1-intc' (alt '') type 5 MMIO   arguments 40000000 256
unusable memory 7e300000 256
device 'mmcnr@7e300000' driver 'brcm,bcm2835-mmc' (alt 'brcm,bcm2835-sdhci') type 5 MMIO   arguments 7e300000 256
device 'wifi@1' driver 'brcm,bcm4329-fmac' (alt '') type 5 MMIO   arguments 1 0
unusable memory 7e00b840 60
device 'mailbox@7e00b840' driver 'brcm,bcm2836-vchiq' (alt 'brcm,bcm2835-vchiq') type 5 MMIO   arguments 7e00b840 60
unusable memory 7e200000 4096
device 'gpiomem' driver 'brcm,bcm2835-gpiomem' (alt '') type 5 MMIO   arguments 7e200000 4096
device 'cpu@0' driver 'arm,cortex-a53' (alt '') type 1 CPU    arguments d8 0
device 'cpu@1' driver 'arm,cortex-a53' (alt '') type 1 CPU    arguments e0 1
device 'cpu@2' driver 'arm,cortex-a53' (alt '') type 1 CPU    arguments e8 2
device 'cpu@3' driver 'arm,cortex-a53' (alt '') type 1 CPU    arguments f0 3
Done.
```

Raspberry Pi 4
--------------

```
$ ./test bcm2711-rpi-4-b.dtb
unusable memory 0 4096
unusable memory 7e101000 8192
device 'cprman@7e101000' driver 'brcm,bcm2711-cprman' (alt '') type 5 MMIO   arguments 7e101000 8192
unusable memory 7e00b880 64
device 'mailbox@7e00b880' driver 'brcm,bcm2835-mbox' (alt '') type 5 MMIO   arguments 7e00b880 64
device 'mailbox@7e00b880' driver 'brcm,bcm2835-mbox' (alt '') type 3 IRQ    arguments 21 4
unusable memory 7e200000 180
device 'gpio@7e200000' driver 'brcm,bcm2711-gpio' (alt '') type 5 MMIO   arguments 7e200000 180
device 'gpio@7e200000' driver 'brcm,bcm2711-gpio' (alt '') type 3 IRQ    arguments 71 4
device 'gpio@7e200000' driver 'brcm,bcm2711-gpio' (alt '') type 3 IRQ    arguments 72 4
unusable memory 7e201000 512
device 'serial@7e201000' driver 'arm,pl011' (alt 'arm,primecell') type 5 MMIO   arguments 7e201000 512
device 'serial@7e201000' driver 'arm,pl011' (alt 'arm,primecell') type 3 IRQ    arguments 79 4
device 'spidev@0' driver 'spidev' (alt '') type 5 MMIO   arguments 0 0
device 'spidev@1' driver 'spidev' (alt '') type 5 MMIO   arguments 1 0
unusable memory 7e215000 8
device 'aux@7e215000' driver 'brcm,bcm2835-aux' (alt '') type 5 MMIO   arguments 7e215000 8
unusable memory 7e215040 64
device 'serial@7e215040' driver 'brcm,bcm2835-aux-uart' (alt '') type 5 MMIO   arguments 7e215040 64
device 'serial@7e215040' driver 'brcm,bcm2835-aux-uart' (alt '') type 3 IRQ    arguments 5d 4
unusable memory 40000000 256
device 'local_intc@40000000' driver 'brcm,bcm2836-l1-intc' (alt '') type 5 MMIO   arguments 40000000 256
unusable memory 40041000 4096
device 'interrupt-controller@40041000' driver 'arm,gic-400' (alt '') type 5 MMIO   arguments 40041000 4096
unusable memory 40042000 8192
device 'interrupt-controller@40041000' driver 'arm,gic-400' (alt '') type 5 MMIO   arguments 40042000 8192
unusable memory 40044000 8192
device 'interrupt-controller@40041000' driver 'arm,gic-400' (alt '') type 5 MMIO   arguments 40044000 8192
unusable memory 40046000 8192
device 'interrupt-controller@40041000' driver 'arm,gic-400' (alt '') type 5 MMIO   arguments 40046000 8192
device 'interrupt-controller@40041000' driver 'arm,gic-400' (alt '') type 3 IRQ    arguments 100000009 3844
unusable memory 7d5d2000 3840
device 'avs-monitor@7d5d2000' driver 'brcm,bcm2711-avs-monitor' (alt 'syscon') type 5 MMIO   arguments 7d5d2000 3840
unusable memory 7e007000 2816
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 5 MMIO   arguments 7e007000 2816
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 50 4
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 51 4
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 52 4
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 53 4
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 54 4
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 55 4
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 56 4
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 57 4
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 57 4
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 58 4
device 'dma@7e007000' driver 'brcm,bcm2835-dma' (alt '') type 3 IRQ    arguments 58 4
unusable memory 7e100000 276
device 'watchdog@7e100000' driver 'brcm,bcm2711-pm' (alt 'brcm,bcm2835-pm-wdt') type 5 MMIO   arguments 7e100000 276
unusable memory 7e00a000 36
device 'watchdog@7e100000' driver 'brcm,bcm2711-pm' (alt 'brcm,bcm2835-pm-wdt') type 5 MMIO   arguments 7e00a000 36
unusable memory 7ec11000 32
device 'watchdog@7e100000' driver 'brcm,bcm2711-pm' (alt 'brcm,bcm2835-pm-wdt') type 5 MMIO   arguments 7ec11000 32
unusable memory 7e104000 40
device 'rng@7e104000' driver 'brcm,bcm2711-rng200' (alt '') type 5 MMIO   arguments 7e104000 40
unusable memory 7e300000 256
device 'mmcnr@7e300000' driver 'brcm,bcm2835-mmc' (alt 'brcm,bcm2835-sdhci') type 5 MMIO   arguments 7e300000 256
device 'mmcnr@7e300000' driver 'brcm,bcm2835-mmc' (alt 'brcm,bcm2835-sdhci') type 3 IRQ    arguments 7e 4
unusable memory 7e00b840 60
device 'mailbox@7e00b840' driver 'brcm,bcm2711-vchiq' (alt '') type 5 MMIO   arguments 7e00b840 60
device 'mailbox@7e00b840' driver 'brcm,bcm2711-vchiq' (alt '') type 3 IRQ    arguments 22 4
unusable memory 7e200000 4096
device 'gpiomem' driver 'brcm,bcm2835-gpiomem' (alt '') type 5 MMIO   arguments 7e200000 4096
unusable memory 7e340000 256
device 'mmc@7e340000' driver 'brcm,bcm2711-emmc2' (alt '') type 5 MMIO   arguments 7e340000 256
device 'mmc@7e340000' driver 'brcm,bcm2711-emmc2' (alt '') type 3 IRQ    arguments 7e 4
device 'cpu@0' driver 'arm,cortex-a72' (alt '') type 1 CPU    arguments d8 0
device 'cpu@1' driver 'arm,cortex-a72' (alt '') type 1 CPU    arguments e0 1
device 'cpu@2' driver 'arm,cortex-a72' (alt '') type 1 CPU    arguments e8 2
device 'cpu@3' driver 'arm,cortex-a72' (alt '') type 1 CPU    arguments f0 3
unusable memory 7d500000 37648
device 'pcie@7d500000' driver 'brcm,bcm2711-pcie' (alt '') type 5 MMIO   arguments 7d500000 37648
device 'pcie@7d500000' driver 'brcm,bcm2711-pcie' (alt '') type 3 IRQ    arguments 93 4
device 'pcie@7d500000' driver 'brcm,bcm2711-pcie' (alt '') type 3 IRQ    arguments 94 4
unusable memory 7d580000 65536
device 'ethernet@7d580000' driver 'brcm,bcm2711-genet-v5' (alt '') type 5 MMIO   arguments 7d580000 65536
device 'ethernet@7d580000' driver 'brcm,bcm2711-genet-v5' (alt '') type 3 IRQ    arguments 9d 4
device 'ethernet@7d580000' driver 'brcm,bcm2711-genet-v5' (alt '') type 3 IRQ    arguments 9e 4
unusable memory e14 8
device 'mdio@e14' driver 'brcm,genet-mdio-v5' (alt '') type 5 MMIO   arguments e14 8
unusable memory 7e007b00 1024
device 'dma@7e007b00' driver 'brcm,bcm2711-dma' (alt '') type 5 MMIO   arguments 7e007b00 1024
device 'dma@7e007b00' driver 'brcm,bcm2711-dma' (alt '') type 3 IRQ    arguments 59 4
device 'dma@7e007b00' driver 'brcm,bcm2711-dma' (alt '') type 3 IRQ    arguments 5a 4
device 'dma@7e007b00' driver 'brcm,bcm2711-dma' (alt '') type 3 IRQ    arguments 5b 4
device 'dma@7e007b00' driver 'brcm,bcm2711-dma' (alt '') type 3 IRQ    arguments 5c 4
unusable memory 7eb10000 4096
device 'codec@7eb10000' driver 'raspberrypi,rpivid-vid-decoder' (alt '') type 5 MMIO   arguments 7eb10000 4096
unusable memory 7eb00000 65536
device 'codec@7eb10000' driver 'raspberrypi,rpivid-vid-decoder' (alt '') type 5 MMIO   arguments 7eb00000 65536
device 'codec@7eb10000' driver 'raspberrypi,rpivid-vid-decoder' (alt '') type 3 IRQ    arguments 62 4
Done.
```
